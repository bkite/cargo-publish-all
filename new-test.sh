#!/bin/sh

set -e

if [ "$1" = "" ]; then
    echo "Missing crate name";
    exit 1
fi;

if [ "$2" = "" ]; then
    echo "Missing test name";
    exit 1
fi;

crate_name="$1"
crate_dir="tests/crates/$1"
test_name="$2"
command="./target/debug/cargo-publish-all --allow-dirty --dry-run ${*:3}"

if [ ! -d "$crate_dir" ]; then
    # Random; makes sure we're not pulling from crates.io by accident
    postfix="gebkq1upwx07qlul"

    cd tests/crates/;
    cargo new --lib $crate_name --name "${crate_name}-${postfix}"
    cd ../..
fi

mkdir -p ${crate_dir}/assertions

cargo build --bin cargo-publish-all &> /dev/null

cd $crate_dir

r=0
{
  out=$(sh -c "../../../$command" /x 2> /dev/fd/3 || true)
  err=$(cat<&3)
} 3<<EOF
EOF

cd ../../..

# Note: exit code does not yet work

N=$'\n'
ron="($N    args: \"${*:3}\",$N    exit_code: ${r},$N    stdout: \"$N${out}$N\",$N    stderr: \"$N${err}$N\",$N)"

ron_file=${crate_dir}/assertions/${test_name}.ron
[ -e "$ron_file" ] && rm $ron_file
echo "$ron" >> ${ron_file}
