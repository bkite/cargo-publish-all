use std::time::Duration;

use serde::Deserialize;

use crate::RegistryError;

const REGISTRY_HOST: &str = "https://crates.io";

#[derive(Deserialize)]
pub struct Versions {
    pub versions: Vec<CrateVersion>,
}

#[derive(Deserialize)]
pub struct CrateVersion {
    #[serde(rename = "crate")]
    pub name: String,
    #[serde(rename = "num")]
    pub version: semver::Version,
    pub yanked: bool,
}

pub fn fetch_cratesio(crate_name: &str) -> std::result::Result<Versions, RegistryError> {
    let url = format!(
        "{host}/api/v1/crates/{crate_name}",
        host = REGISTRY_HOST,
        crate_name = crate_name
    );

    match get_with_timeout(&url) {
        Ok(response) => Ok(serde_json::from_reader(response)?),
        Err(e) => {
            let not_found_error = e.status() == Some(reqwest::StatusCode::NOT_FOUND);

            if not_found_error {
                return Ok(Versions { versions: vec![] });
            }

            Err(e.into())
        }
    }
}

fn get_with_timeout(url: &str) -> reqwest::Result<reqwest::Response> {
    let timeout = Duration::from_secs(10);

    let client = reqwest::ClientBuilder::new().timeout(timeout).build()?;

    client
        .get(url)
        .send()
        .and_then(|resp| resp.error_for_status())
}
